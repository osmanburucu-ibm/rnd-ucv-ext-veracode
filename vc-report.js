const https = require("https");
const auth = require("./vc-auth");

var options2 = {
    host: auth.getHost(),
    path: "/appsec/v2/applications",
    method: "GET"
}

var getSummaryReport = (properties) => {
    var results = "";
    console.log('getSummaryReport');
    options2.path = options2.path + "/" + properties.app_guid + "/" + "summary_report"
    console.log ('path=' + options2.path)
    properties.options2 = options2;
    makeCall2(properties, function(results){
        // console.log('results:',results);
        properties.results = results;
        handleResults2(properties);        
    });

    return 0;
}

function makeCall2 (properties, callback) {
    console.log('makeCall2');
    properties.options2.headers = {
        "Authorization": auth.generateHeader(properties, properties.options2.path, properties.options2.method)
    }
    console.log("options2.authorization="+properties.options2.headers["Authorization"])
    console.log("options2.path=" + properties.options2.path)
    var req = https.request(properties.options2,function (res) {
        var body = "";

        res.on("data", (chunk) => {
            console.log('on data')
            body += chunk;
        });

        res.on('end', function (d) {
            console.log('on end')
            // console.log(body)
            callback(body);
        });
        res.on('error', function (e) {
            console.log('on error')
            console.error(e);
        });
    });
    req.on("error", (err) => {
        console.error(err);
    });

    req.end();    
}

function handleResults2(properties){
    console.log('handleResults2');
    var summaryReport = JSON.parse(properties.results);

    // 
    // summaryReport.last_update_time
    // summaryReport.flaw-status.sev-5-change	==> HIGH

    console.log ('last_update_time: ');
    console.log (summaryReport.last_update_time);

}

module.exports = {
	getSummaryReport
}