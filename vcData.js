const https = require('https')
const auth = require('./vc-auth')


var options = {
  host: auth.getHost(),
  path: '/appsec/v1/applications?size=100&page=0',
  method: 'GET'
}

var vcproperties = {
    api_id: "",
    api_key: "",
    app_name: "",
    app_guid: ""
}


function makeCall (properties) {
  return new Promise((resolve, reject) => {
    console.log('makeCall')
    options.headers = {
      'Authorization': auth.generateHeader(properties, options.path, options.method)
    }
    properties.options = options
    https.request(properties.options, (res) => {
      let chunks_of_data = [];
      res.on('data', (fragments) => {
        chunks_of_data.push(fragments);
      });
      res.on('end', () => {
        let response_body = Buffer.concat(chunks_of_data);
        resolve(response_body.toString());
      });
      res.on('error', (error) => {
        reject(error);
      });
    });
  });
}

async function makeSyncReq (properties, request) {
  try {
    let http_promise = makeCall(properties);
    let response_body = http_promise;
    console.log (response_body);
  }
  catch(error) {
    console.log(error);
  }
}

//export default class VCData {
  module.exports = 
    class VCData {
    // static async initialize (apiid, apikey, appname ) {
    //   if (!this.vcproperties) {
    //     console.log('Initializing VCData.')
    //     try {
    //       this.vcproperties.api_id = apiid;
    //       this.vcproperties.api_key = apikey;
    //       this.app_name = appname;
    //     } catch (error) {
    //       console.log(error)
    //       throw error
    //     }
    //   }
    // }
    static async getAPPid (properties) {
      console.log ('before make sync req')
      await makeSyncReq (properties);
      console.log ('after make sync req')
    }
}