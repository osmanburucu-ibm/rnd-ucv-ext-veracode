var properties = {
    appName: process.env.APP_NAME,
    app_guid: "",
    api_id: process.env.API_ID, // your API ID, reading from environment variable
    api_key: process.env.KEY // your API key, reading from environment variable}
}

module.exports = {
    properties
}