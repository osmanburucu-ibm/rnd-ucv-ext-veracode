const https = require("https");
const auth = require("./vc-auth");
const summaryreport = require ("./vc-report");
//import VCData from './vcData';
const VCData = require ('./vcData');


var options = {
    host: auth.getHost(),
    path: "/appsec/v1/applications?name=",
    method: "GET"
}

var getApplications = (properties) => {
    console.log('getApplications');
   // VCData.initialize (properties.api_id, properties.api_key, properties.appName);
  //  await VCData.getAPPid(properties);
    
    var body = "";
//    body = req.end();
	return body;
}

var getAppID = (properties) => {
    var results = "";
    console.log('getAppID');
    options.path=options.path + properties.appName
    properties.options = options;
    makeCall(properties, function(results){
        // console.log('results:',results);
        properties.results = results;
        handleResults(properties);        
    });

    return 0;
}

function makeCall (properties, callback) {
    console.log('makeCall');
    options.headers = {
        "Authorization": auth.generateHeader(properties, options.path, options.method)
    }
    var req = https.request(properties.options,function (res) {
        var body = "";

        res.on("data", (chunk) => {
            console.log('on data')
            body += chunk;
        });

        res.on('end', function (d) {
            console.log('on end')
            // console.log(body)
            callback(body);
        });
        res.on('error', function (e) {
            console.log('on error')
            console.error(e);
        });
    });
    req.on("error", (err) => {
        console.error(err);
    });

    req.end();    
}

function handleResults(properties){
    console.log('handleResults');
    var allApps = JSON.parse(properties.results);

    // name of app is in : object►_embedded►applications►[]►profile►name
    // guid is in: bject►_embedded►applications►[]►guid
    allApps._embedded.applications.forEach((item) => {
        console.log('ID: ' + item.guid);
        console.log('Name:' + item.profile.name);
        if (item.profile.name == properties.appName) {
            console.log ('found it!!');
            properties.app_guid = item.guid;
        }
    });
    console.log ('guid of app is: ');
    console.log (properties.app_guid);
    summaryreport.getSummaryReport (properties);
}

// makeCall(options, function(results){
//     console.log('makeCall is started')
//     console.log('results:',results);
//     handleResults(results);        
// });
module.exports = {
    getApplications,
    makeCall,
	getAppID
}